package com.example.boom

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.boom.adapter.UsersRecycleViewAdapter
import com.example.boom.response.UserResponse


class TrackListDialogFragment(
    private var dataList: ArrayList<Pair<UserResponse, Boolean>>
) : DialogFragment(){


    interface OnFuncListener {
        fun onTrackList(userData: ArrayList<Pair<UserResponse, Boolean>>, position: Int)
        fun onDismissCheck(userData: ArrayList<Pair<UserResponse, Boolean>>)
    }

    private var mCallback: OnFuncListener? = null

    fun setOnFuncListener(callback: OnFuncListener) {
        this.mCallback = callback
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        val view = LayoutInflater.from(context).inflate(R.layout.track_list_layout, null, false)
        val recycleView : RecyclerView? = view?.findViewById(R.id.trackRecycle)
        setTrackRecycleView(recycleView, dataList)
        mBehaviorSet(view,dialog)
        return dialog
    }

    override fun dismiss() {
        mCallback?.onDismissCheck(dataList)
        super.dismiss()

    }

    private fun mBehaviorSet(view : View, dialog:Dialog){
        dialog.setContentView(view)

        val layoutParams = view.layoutParams
        val width = (dialog.context.resources.displayMetrics.widthPixels )
        val height = (dialog.context.resources.displayMetrics.heightPixels )

        layoutParams.width = width
        layoutParams.height = height
        view.layoutParams = layoutParams

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
    }
    override fun onDestroy() {
        super.onDestroy()
    }


    private fun setTrackRecycleView(
        recycleView: RecyclerView?,
        list: ArrayList<Pair<UserResponse, Boolean>>
    ) {
        val mLayoutManager = GridLayoutManager(context, 1)
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        val adapter = context?.let { UsersRecycleViewAdapter(it, list) }
        recycleView?.layoutManager = mLayoutManager
        recycleView!!.adapter = adapter

        val callback = object : UsersRecycleViewAdapter.OnFuncListener {
            override fun onItemClick(
                list: ArrayList<Pair<UserResponse, Boolean>>,
                position: Int
            ) {
                dataList = list
                mCallback?.onTrackList(list, position)
            }

            override fun onClickFavorite(
                list: ArrayList<Pair<UserResponse, Boolean>>,
                position: Int
            ) {
                dataList = list
                mCallback?.onDismissCheck(list)
            }
        }
        adapter?.setOnFuncListener(callback)
    }

}
