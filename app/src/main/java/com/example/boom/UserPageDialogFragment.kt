package com.example.boom

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.example.boom.response.OneUserResponse
import android.text.Html
import android.text.method.LinkMovementMethod


class UserPageDialogFragment(private var oneUserResponse: Pair<OneUserResponse, Boolean>) : DialogFragment(){


    interface OnFuncListener {
        fun onDismissUpdate(favoriteItem: Pair<OneUserResponse, Boolean>)
        fun onIsFavorite(favoriteItem: Pair<OneUserResponse, Boolean>)
    }

    private var mCallback: OnFuncListener? = null

    fun setOnFuncListener(callback: OnFuncListener) {
        this.mCallback = callback
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        val view = LayoutInflater.from(context).inflate(R.layout.user_detail, null, false)
        val back : ImageView? = view?.findViewById(R.id.back)
        val headShot : ImageView? = view?.findViewById(R.id.userDetailHeadShot)
        val name : TextView? =view?.findViewById(R.id.name)
        val bio : TextView? =view?.findViewById(R.id.bio)
        val nameInfo : View? =view?.findViewById(R.id.nameInfo)
        val locationInfo : View? =view?.findViewById(R.id.locationInfo)
        val linkInfo : View? =view?.findViewById(R.id.linkInfo)
        val trackList : TextView? = view?.findViewById(R.id.track)
        val isFavoriteTextView : TextView? = view?.findViewById(R.id.isFavorite)

        if (headShot != null) {
            Glide.with(headShot)
                .load(oneUserResponse.first.avatar_url)
                .into(headShot)
        }

        name?.text = oneUserResponse.first.name
        bio?.text = oneUserResponse.first.bio
        nameInfo?.findViewById<ImageView>(R.id.image)?.setImageResource(R.drawable.baseline_person)
        nameInfo?.findViewById<TextView>(R.id.info)?.text = oneUserResponse.first.login
        nameInfo?.findViewById<TextView>(R.id.tag)?.isEnabled = oneUserResponse.first.site_admin

        locationInfo?.findViewById<ImageView>(R.id.image)?.setImageResource(R.drawable.baseline_location)
        locationInfo?.findViewById<TextView>(R.id.info)?.text = oneUserResponse.first.location


        val webLinkText = "<a href = '${oneUserResponse.first.blog}'> ${oneUserResponse.first.blog} </a>"
        linkInfo?.findViewById<ImageView>(R.id.image)?.setImageResource(R.drawable.baseline_link)
        linkInfo?.findViewById<TextView>(R.id.info)?.text = Html.fromHtml(webLinkText)
        linkInfo?.findViewById<TextView>(R.id.info)?.setMovementMethod(LinkMovementMethod.getInstance())

        if ( oneUserResponse.second)   isFavoriteTextView?.setBackgroundResource(R.drawable.favorite_bg)
        else  isFavoriteTextView?.setBackgroundResource(R.drawable.non_favorite_bg)
        mBehaviorSet(view,dialog)

        back?.setOnClickListener {
            dismiss()
        }


        trackList?.setOnClickListener {
            mCallback?.onIsFavorite(oneUserResponse)
            dismiss()
        }

        isFavoriteTextView?.setOnClickListener {
            if ( !oneUserResponse.second)   isFavoriteTextView.setBackgroundResource(R.drawable.favorite_bg)
            else  isFavoriteTextView.setBackgroundResource(R.drawable.non_favorite_bg)

            oneUserResponse = Pair(oneUserResponse.first, !oneUserResponse.second)
        }

        return dialog
    }

    override fun dismiss() {
        super.dismiss()
        mCallback?.onDismissUpdate(oneUserResponse)
    }

    private fun mBehaviorSet(view : View, dialog:Dialog){
        dialog.setContentView(view)

        val layoutParams = view.layoutParams
        val width = (dialog.context.resources.displayMetrics.widthPixels )
        val height = (dialog.context.resources.displayMetrics.heightPixels )

        layoutParams.width = width
        layoutParams.height = height
        view.layoutParams = layoutParams

        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.WHITE))
    }
    override fun onDestroy() {
        super.onDestroy()
    }

}
