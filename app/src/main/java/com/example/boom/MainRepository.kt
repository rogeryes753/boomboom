package com.example.boom

class MainRepository constructor(private val retrofitService: RetrofitService) {

    fun getAllUsers() = retrofitService.getAllUsers()
    fun getOneUsers(username : String) = retrofitService.getOneUsers(username)
    fun getTrackList(username : String) = retrofitService.getTrackList(username)
}