package com.example.boom

import com.example.boom.response.OneUserResponse
import com.example.boom.response.UserResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface RetrofitService {

    @GET("/users")
    fun getAllUsers() : Call<MutableList<UserResponse>>


    @GET("/users/{username}")
    fun getOneUsers(
        @Path("username") username : String
    ) : Call<OneUserResponse>


    @GET("  /users/{username}/followers")
    fun getTrackList(
        @Path("username") username : String
    ): Call<MutableList<UserResponse>>

    companion object {

        var retrofitService: RetrofitService? = null

        fun getInstance() : RetrofitService {

            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.github.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }
}