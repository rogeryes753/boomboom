package com.example.boom

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.boom.adapter.UsersRecycleViewAdapter
import com.example.boom.databinding.MainLayoutBinding
import com.example.boom.response.OneUserResponse
import com.example.boom.response.UserResponse
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class MainActivity : AppCompatActivity() {
    lateinit var mainViewModel: MainViewModel
    var adapter : UsersRecycleViewAdapter? = null
    private val retrofitService = RetrofitService.getInstance()
//    var favoriteArray : ArrayList<Pair<UserResponse, Boolean>> = ArrayList()
    var favoritePosition = -1
    var trackArray : ArrayList<Pair<UserResponse, Boolean>> = ArrayList()
    private var sharedPreferences: SharedPreferences? = null
    //    var trackArray :ArrayList<Pair<Boolean> = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: MainLayoutBinding = DataBindingUtil.setContentView(this, R.layout.main_layout)
//        mainViewModel = ViewModelProvider.AndroidViewModelFactory(application).create(MainViewModel::class.java)
        mainViewModel = ViewModelProvider(this, MyViewModelFactory(MainRepository(retrofitService))).get(MainViewModel::class.java)
        binding.viewModel = mainViewModel
        binding.lifecycleOwner = this
        val recyclerView : RecyclerView = binding.recy1
        sharedPreferences = this.getSharedPreferences("kotlinsharedpreference", Context.MODE_PRIVATE)
        if(getSharedPreferences() != null ) {
            mainViewModel.updateFavorArray(getSharedPreferences()!!)
//            mainViewModel.favoriteArray = getSharedPreferences()!!
            Log.d("favoriteArray", "${mainViewModel.favoriteArray}")
        }

        mainViewModel.userList.observe(this, Observer {
            Log.d("userListResponse", "$it")
            val favorArray = mainViewModel.favoriteArray.value ?: return@Observer
            if (it == null ) return@Observer
            favorArray.let { it1 ->
                mainViewModel.updateUserListToFavoriteArray(it,
                    it1
                )
            }

            setRecyclerView(recyclerView, it,  favorArray)
//            it.forEach { item -> favoriteArray.add(Pair(item, false)) }
//            setRecyclerView(recyclerView, it, favoriteArray)

        })


        mainViewModel.oneUserInfo.observe(this, Observer {
            val favorArray =  mainViewModel.favoriteArray.value
            Log.d("oneUserResponse", "$it")
            if (it != null && favorArray != null) {
                setUserPage(Pair(it, favorArray[favoritePosition].second))
            }
        })

        mainViewModel.trackList.observe(this, Observer {
            val favorArray =  mainViewModel.favoriteArray.value
            Log.d("trackListResponse", "$it")
            if (it != null&& favorArray != null ) {
                trackArray = mainViewModel.setTrackListFavorite(it, favorArray)
                setTrackList(trackArray)
            }
        })

        mainViewModel.errorMessage.observe(this, {
            Log.d("errorMessage", "$it")
        })

        mainViewModel.getAllUsers()
//        setAPIClient(recyclerView)
    }


    override fun onPause() {
        super.onPause()
        val favorArray =  mainViewModel.favoriteArray.value
        if (favorArray != null) {
            putSharedPreferences(favorArray)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
//        putSharedPreferences(favoriteArray)
    }

    private fun setRecyclerView(
        recyclerView: RecyclerView?,
        usersResponse: MutableList<UserResponse>?,
        favoriteArray: ArrayList<Pair<UserResponse, Boolean>>
    ) {
        val mLayoutManager = GridLayoutManager(this, 1)
        mLayoutManager.orientation = LinearLayoutManager.VERTICAL
        adapter = UsersRecycleViewAdapter(this, favoriteArray)
        recyclerView?.layoutManager = mLayoutManager
        recyclerView!!.adapter = adapter

        val callback = object : UsersRecycleViewAdapter.OnFuncListener {
            override fun onItemClick(
                list: ArrayList<Pair<UserResponse, Boolean>>,
                position: Int
            ) {
//                this@MainActivity.favoriteArray = list
                mainViewModel.updateFavorArray(list)
//                Log.d("UsersRecycleViewAdapter", "${this@MainActivity.favoriteArray}")
                favoritePosition = position
                mainViewModel.getOneUser(list[position].first.login)
            }

            override fun onClickFavorite(
                list: ArrayList<Pair<UserResponse, Boolean>>,
                position: Int
            ) {
                mainViewModel.updateFavorArray(list)
                adapter?.updateTrackItem(mainViewModel.favoriteArray.value!!)
            }
        }
        adapter?.setOnFuncListener(callback)
    }



    private fun setUserPage(
        oneUserResponse: Pair<OneUserResponse, Boolean>
    ) {
        val manager = this.supportFragmentManager
        val userPageDialogFragment = UserPageDialogFragment(oneUserResponse)
        userPageDialogFragment?.show(manager,"userPageDialogFragment")
        val callBack = object : UserPageDialogFragment.OnFuncListener {

            override fun onDismissUpdate(favoriteItem: Pair<OneUserResponse, Boolean>) {
                val favorArray = mainViewModel.favoriteArray.value ?: return
                mainViewModel.updateFavorArrayOneItem(favoritePosition, favoriteItem.second)
                adapter?.updateTrackItem(favorArray)
            }

            override fun onIsFavorite(favoriteItem: Pair<OneUserResponse, Boolean>) {
                val favorArray = mainViewModel.favoriteArray.value ?: return
                mainViewModel.getTrackList(favoriteItem.first.login)
                mainViewModel.updateFavorArrayOneItem(favoritePosition, favoriteItem.second)
//                this@MainActivity.favoriteArray[favoritePosition] = Pair(this@MainActivity.favoriteArray[favoritePosition].first , favoriteItem.second )
                adapter?.updateTrackItem(favorArray)
            }

        }
        userPageDialogFragment.setOnFuncListener(callBack)
    }

    private fun setTrackList(
        array: ArrayList<Pair<UserResponse, Boolean>>
    ) {
        val manager = this.supportFragmentManager
        val trackListDialogFragment = TrackListDialogFragment(array)
        trackListDialogFragment.show(manager, "trackListDialogFragment")
        val callBack = object : TrackListDialogFragment.OnFuncListener {

            override fun onTrackList(
                userData: ArrayList<Pair<UserResponse, Boolean>>,
                position: Int
            ) {
                val favoriteArray = mainViewModel.favoriteArray.value ?: return
                mainViewModel.updateFavoriteArrayToTrackArray(userData, favoriteArray)
                adapter?.updateTrackItem(favoriteArray)
                trackArray = userData
                mainViewModel.getOneUser(userData[position].first.login)
            }

            override fun onDismissCheck(userData: ArrayList<Pair<UserResponse, Boolean>>) {
                val favoriteArray = mainViewModel.favoriteArray.value ?: return
                mainViewModel.updateFavoriteArrayToTrackArray(userData, favoriteArray)
                adapter?.updateTrackItem(favoriteArray)
            }

        }

        trackListDialogFragment.setOnFuncListener(callBack)
    }


    private fun putSharedPreferences(list:ArrayList<Pair<UserResponse, Boolean>>){
        val favoriteArray = mainViewModel.favoriteArray.value ?: return
        Log.d("favoriteArray", "putSharedPreferences + $favoriteArray")
        val gson = Gson()
        val json = gson.toJson(list)//converting list to Json
        val editor: SharedPreferences.Editor? =  sharedPreferences?.edit()
        editor?.putString("LIST",json)
        editor?.apply()
        editor?.commit()
    }

    fun getSharedPreferences(): ArrayList<Pair<UserResponse, Boolean>>? {
        val gson = Gson()
        val json = sharedPreferences?.getString("LIST",null) ?: return null
        val type = object : TypeToken<ArrayList<Pair<UserResponse, Boolean>>>(){}.type//converting the json to list
        Log.d("favoriteArray", "getSharedPreferences + ${type}")
        return gson.fromJson(json,type)//returning the list
    }

}

