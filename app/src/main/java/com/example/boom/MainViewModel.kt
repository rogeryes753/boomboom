package com.example.boom

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.boom.response.OneUserResponse
import com.example.boom.response.UserResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainViewModel constructor(private val repository: MainRepository)  : ViewModel() {

    val userList = MutableLiveData<MutableList<UserResponse>>()
    val oneUserInfo = MutableLiveData<OneUserResponse>()
    val trackList = MutableLiveData<MutableList<UserResponse>>()
    val errorMessage = MutableLiveData<String>()
    var favoriteArray = MutableLiveData<ArrayList<Pair<UserResponse, Boolean>>>()
    fun getAllUsers() {
        val response = repository.getAllUsers()
        response.enqueue(object : Callback<MutableList<UserResponse>> {
            override fun onResponse(call: Call<MutableList<UserResponse>>, response: Response<MutableList<UserResponse>>) {
                userList.postValue(response.body())
            }

            override fun onFailure(call: Call<MutableList<UserResponse>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    fun getOneUser(userName : String) {
        val response = repository.getOneUsers(userName)
        response.enqueue(object : Callback<OneUserResponse> {
            override fun onResponse(call: Call<OneUserResponse>, response: Response<OneUserResponse>) {
                oneUserInfo.postValue(response.body())
            }
            override fun onFailure(call: Call<OneUserResponse>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    fun getTrackList(userName : String) {
        val response = repository.getTrackList(userName)
        response.enqueue(object : Callback<MutableList<UserResponse>> {
            override fun onResponse(call: Call<MutableList<UserResponse>>, response: Response<MutableList<UserResponse>>) {
                trackList.postValue(response.body())
            }
            override fun onFailure(call: Call<MutableList<UserResponse>>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

    fun setTrackListFavorite(
        trackList: MutableList<UserResponse>,
        favoriteArray: ArrayList<Pair<UserResponse, Boolean>>
    ): ArrayList<Pair<UserResponse, Boolean>>{
        val newArrayList : ArrayList<Pair<UserResponse, Boolean>> = ArrayList()
        trackList.forEach outer@{ trackItem ->
            var isFind = false
            favoriteArray.forEach inner@{ favoriteItem ->
                if (favoriteItem.first.login == trackItem.login) {
                    newArrayList.add(Pair(trackItem, favoriteItem.second))
                    isFind = true
                }
            }
            if (!isFind) newArrayList.add(Pair(trackItem, false))
        }
        return newArrayList
    }

    fun updateFavoriteArrayToTrackArray(
        trackList: ArrayList<Pair<UserResponse, Boolean>>,
        favorArray: ArrayList<Pair<UserResponse, Boolean>>
    ) {
        favorArray.forEachIndexed { index, newItem ->
            trackList.forEach { trackItem ->
                if (newItem.first.login == trackItem.first.login) favorArray[index] =
                    Pair(favorArray[index].first, trackItem.second)
            }
        }

        favoriteArray.postValue(favorArray)
    }


    fun updateUserListToFavoriteArray(
        userList: MutableList<UserResponse>,
        favorArray: ArrayList<Pair<UserResponse, Boolean>>
    ) {
        val newArray : ArrayList<Pair<UserResponse, Boolean>> = ArrayList()
        userList.forEach { apiItem ->
            var isAdd = false
            favorArray.forEach { favoriteItem ->
                if (apiItem.login == favoriteItem.first.login) {
                    newArray.add(favoriteItem)
                    isAdd = true
                }
            }

            if (!isAdd) newArray.add(Pair(apiItem, false))
        }

        favoriteArray.postValue(newArray)
    }


    fun updateFavorArray(favorArray: ArrayList<Pair<UserResponse, Boolean>>) {
        favoriteArray.postValue(favorArray)
    }


    fun updateFavorArrayOneItem(favoritePosition: Int, isFavorite : Boolean) {
        val array : ArrayList<Pair<UserResponse, Boolean>> = favoriteArray.value ?: return
        array[favoritePosition] =  Pair(array[favoritePosition].first , isFavorite )
    }
}