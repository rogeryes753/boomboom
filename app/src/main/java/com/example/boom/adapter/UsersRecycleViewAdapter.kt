package com.example.boom.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.example.boom.R
import com.example.boom.response.UserResponse

class UsersRecycleViewAdapter(
    private val context: Context,
    private var dataList: ArrayList<Pair<UserResponse, Boolean>>
): RecyclerView.Adapter<UsersRecycleViewAdapter.PageHolder>(){

    /** interface **/
    interface OnFuncListener {
        fun onItemClick(list: ArrayList<Pair<UserResponse, Boolean>>, position: Int)
        fun onClickFavorite(list: ArrayList<Pair<UserResponse, Boolean>>, position: Int)
    }

    private var mCallback: OnFuncListener? = null

    fun setOnFuncListener(callback: OnFuncListener) {
        this.mCallback = callback
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PageHolder {
        return PageHolder(LayoutInflater.from(context).inflate(R.layout.recycle_item, parent, false))
    }

    override fun onBindViewHolder(holder: PageHolder, position: Int) {

        if ( dataList[position].second)   holder.favoriteButton.setBackgroundResource(R.drawable.favorite_bg)
        else  holder.favoriteButton.setBackgroundResource(R.drawable.non_favorite_bg)

        val roundedCorner = RoundedCorners(25)
        val options = RequestOptions.bitmapTransform(roundedCorner).override(400, 400)
        Glide.with(holder.headShot)
            .load(dataList?.get(position).first.avatar_url)
            .apply(options)
            .into(holder.headShot)

        holder.loginName.text = dataList?.get(position).first.login

        holder.isAdmin.isEnabled = dataList!![position].first.site_admin

        holder.itemLayout.setOnClickListener {
            mCallback?.onItemClick(dataList, position)
        }

        holder.favoriteButton.setOnClickListener {
//            holder.favoriteButton = !dataList[position].second
            if ( !dataList[position].second)   holder.favoriteButton.setBackgroundResource(R.drawable.favorite_bg)
            else  holder.favoriteButton.setBackgroundResource(R.drawable.non_favorite_bg)
            dataList[position] = Pair(dataList[position].first, !dataList[position].second)
            Log.d("favoriteButton", "$dataList")
            mCallback?.onClickFavorite(dataList, position)
        }

    }

    override fun getItemCount(): Int {
        if (dataList == null) return 0
        return dataList.size
    }

    inner class PageHolder(view: View): RecyclerView.ViewHolder(view){
        val headShot: ImageView = view.findViewById(R.id.headShot)
        val loginName: TextView = view.findViewById(R.id.loginName)
        val isAdmin: TextView = view.findViewById(R.id.isAdmin)
        val itemLayout : View = view.findViewById(R.id.itemLayout)
        val favoriteButton : TextView = view.findViewById(R.id.favoriteButton)
    }


    fun updateTrackItem( newDataList : ArrayList<Pair<UserResponse, Boolean>>) {
        dataList = newDataList
        notifyDataSetChanged()
    }
}